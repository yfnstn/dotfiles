#!/usr/bin/env zsh

fpath=($ZDOTDIR/external $fpath)

source "$XDG_CONFIG_HOME/zsh/aliases"

# Vim style completion mapping
# ## must load before compinit ##
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Edit cli in Neovim
# loaded from https://linux.die.net/man/1/zshcontrib
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# Load zsh completions
autoload -U compinit; compinit
# Autocomplete hidden files
_comp_options+=(globdots)
source ~/dotfiles/zsh/external/completion.zsh

# make sure ssh-agent is running and not duplicated
osType=$(uname)
case "$osType" in
    "Linux")
        {
            if ! pgrep -u "$USER" ssh-agent > /dev/null; then
                ssh-agent -t 3h > "$XDG_RUNTIME_DIR/ssh-agent.env"
            fi
            if [[ ! -f "$SSH_AUTH_SOCK" ]]; then
                source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
            fi
        } ;; 
    *)
        {} ;;
esac

# use ctrl+g to clear screen (instead of ctrl+l
bindkey -r '^l'
bindkey -r '^g'
bindkey -s '^g' 'clear\n'

bindkey -v
export KEYTIMEOUT=1

autoload -Uz prompt_purification_setup; prompt_purification_setup
autoload -Uz cursor_mode && cursor_mode

# Push the current directory visited on to the stack
setopt AUTO_PUSHD
# Don't store duplicate directories in stack
setopt PUSHD_IGNORE_DUPS
# Don't print the directory stack after using pushd/popd
setopt PUSHD_SILENT

# fzf check
# if [ $(command -v "fzf") ]; then
#     source /usr/share/fzf/completion.zsh
#    source /usr/share/fzf/key-bindings.zsh
# fi

# auto start i3 window manager
if [ "$(tty)" = "/dev/tty1" ];
then
    pgrep i3 || exec startx "$XDG_CONFIG_HOME/X11/.xinitrc"
fi
 
 
# OPTIONS
# https://zsh.sourceforge.io/Doc/Release/Options.html
setopt AUTO_PARAM_SLASH
unsetopt CASE_GLOB

# PROMPT 
# starship prompt see starship.rs for more info
# eval "$(starship init zsh)"

# ZSH add-ons
# zoxide
# https://github.com/ajeetdsouza/zoxide
eval "$(zoxide init zsh)"

##########################
# ZSH syntax highlighting 
# ## Must source at BOTTOM of zshrc file to apply to everything above it
source ~/dotfiles/zsh/external/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
