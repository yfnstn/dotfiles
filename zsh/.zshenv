#!/usr/bin/env zsh

# For dotfiles
export XDG_CONFIG_HOME="$HOME/.config"

# For specific data
export XDG_DATA_HOME="$XDG_CONFIG_HOME/local/share"

# For cached files
export XDG_CACHE_HOME="$XDG_CONFIG_HOME/cache"

# Define editor of choice
export EDITOR="helix"
export VISUAL="helix"

# # ZDOTDIR config
# export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
# 
# # History filepath
# export HISTFILE="$ZDOTDIR/.zhistory"
# # Max events for internal history
# export HISTSIZE=10000
# # Max events in history file
# export SAVEHIST=10000
# 
# export DOTFILES="$HOME/dotfiles"
# 
# # fzf config to use ripgrep by default
# export FZF_DEFAULT_COMMAND="rg --files --hidden --glob '!.git'"
# export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

